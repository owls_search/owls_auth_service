<?php

use Faker\Generator as Faker;

$factory->define(App\PostTranslation::class, function (Faker $faker) {
    return [
        'title' => $faker->title,
        'text' => $faker->randomHtml(),
        'preview' => $faker->randomHtml(),
        'language' => 'en',
        'post_id' => 1,
        'code' => 'code',
    ];
});
