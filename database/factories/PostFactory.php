<?php

use Faker\Generator as Faker;

$factory->define(App\Post::class, function (Faker $faker) {
    return [
        'image_big'    => $faker->imageUrl(),
        'image_normal' => $faker->imageUrl(),
        'image_small'  => $faker->imageUrl(),
    ];
});
