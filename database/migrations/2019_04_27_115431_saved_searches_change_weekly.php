<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SavedSearchesChangeWeekly extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('saved_searches', function (Blueprint $table) {
            $table->dropColumn('send_day_of_week');
            $table->boolean('send_weekly_monday')->default(false);
            $table->boolean('send_weekly_tuesday')->default(false);
            $table->boolean('send_weekly_wednesday')->default(false);
            $table->boolean('send_weekly_thursday')->default(false);
            $table->boolean('send_weekly_friday')->default(false);
            $table->boolean('send_weekly_saturday')->default(false);
            $table->boolean('send_weekly_sunday')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('saved_searches', function (Blueprint $table) {
            $table->string('send_day_of_week')->nullable();
            $table->dropColumn('send_weekly_monday');
            $table->dropColumn('send_weekly_tuesday');
            $table->dropColumn('send_weekly_wednesday');
            $table->dropColumn('send_weekly_thursday');
            $table->dropColumn('send_weekly_friday');
            $table->dropColumn('send_weekly_saturday');
            $table->dropColumn('send_weekly_sunday');
        });
    }
}
