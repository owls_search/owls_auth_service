<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddLogDetailsTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create(
            'logs_to_details', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('log_id');
            $table->unsignedInteger('detail_id');
            $table->string('value');

            $table->foreign('log_id')
                ->references('id')
                ->on('logs')
                ->onDelete('cascade')
            ;
            $table->foreign('detail_id')
                ->references('id')
                ->on('logs_details')
                ->onDelete('cascade')
            ;
        }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('logs');
    }
}
