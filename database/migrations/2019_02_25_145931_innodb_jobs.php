<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Migrations\Migration;

class InnodbJobs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('ALTER TABLE `jobs` ENGINE = InnoDB;');
        DB::statement('SET autocommit=0; ');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('ALTER TABLE `jobs` ENGINE = MyISAM;');
        DB::statement('SET autocommit=1;');
    }
}
