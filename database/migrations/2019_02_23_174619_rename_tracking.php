<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameTracking extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->renameColumn('tracking_variable', 'partner_tracking_param');
            $table->renameColumn('tracking_value', 'partner_tracking_value');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->renameColumn('partner_tracking_param', 'tracking_variable');
            $table->renameColumn('partner_tracking_value', 'tracking_value');
        });
    }
}
