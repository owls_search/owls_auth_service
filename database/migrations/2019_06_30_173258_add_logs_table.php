<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddLogsTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create(
            'logs', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamp('timestamp');
            $table->unsignedInteger('event_id');

            $table->foreign('event_id')
                ->references('id')
                ->on('logs_events')
                ->onDelete('cascade')
            ;
        }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('logs');
    }
}
