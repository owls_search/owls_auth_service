<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddJobTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jobs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('external_id')->nullable();
            $table->string('url')->nullable();
            $table->text('location', 10000)->nullable();
            $table->text('description', 10000)->nullable();
            $table->text('mini_description')->nullable();
            $table->string('language')->nullable();
            $table->text('skills', 10000)->nullable();
            $table->text('programming', 10000)->nullable();
            $table->string('title')->nullable();
            $table->timestamp('valid_until')->nullable();
            $table->integer('salary_from')->nullable();
            $table->integer('salary_to')->nullable();
            $table->string('email')->nullable();
            $table->string('logo')->nullable();
            $table->string('company_name')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jobs');
    }
}
