<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UsersAddProfileFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('skills')->nullable();
            $table->string('job_titles')->nullable();
            $table->string('job_features')->nullable();
            $table->string('type_of_work')->nullable();
            $table->string('file')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('skills');
            $table->dropColumn('job_titles');
            $table->dropColumn('job_features');
            $table->dropColumn('type_of_work');
            $table->dropColumn('file');
        });
    }
}
