<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPartnerDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->float('partner_bid')->nullable();
        });
        Schema::table('jobs', function (Blueprint $table) {
            $table->float('dynamic_bid')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('partner_bid');
        });
        Schema::table('jobs', function (Blueprint $table) {
            $table->dropColumn('dynamic_bid');
        });
    }
}
