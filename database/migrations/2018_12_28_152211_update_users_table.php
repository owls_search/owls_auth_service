<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('job_features');
            $table->text('skills', 10000)->nullable()->change();
            $table->text('job_titles', 10000)->nullable()->change();
            $table->text('job_locations', 10000)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('job_features')->nullable();
            $table->string('skills')->nullable();
            $table->string('job_titles')->nullable();
            $table->dropColumn('job_locations');
        });
    }
}
