<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class AddSavedSearchFulltextIndex extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::table(
            'saved_searches', function (Blueprint $table) {
            $table->string('send_date_of_month', 100)->nullable()->change();
        }
        );
        DB::statement('ALTER TABLE `saved_searches` ADD FULLTEXT `full`(`send_date_of_month`)');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table(
            'saved_searches', function (Blueprint $table) {
            $table->tinyInteger('send_date_of_month')->nullable()->change();
        }
        );
    }
}
