<?php

// File storage api
Route::post('file/store', 'API\FileStorageController@storeFile');
Route::post('file/delete', 'API\FileStorageController@deleteFile');
Route::get('file/get', 'API\FileStorageController@getFile');

// Mailing api
Route::post('mail/send', 'API\MailingController@internalSendMail');

// Google feed
Route::get('export/google', 'API\ExportController@getGoogleFeed');

//Sitemaps
Route::get('gcs/sitemap/{file}', 'API\FileStorageController@getSitemap');
