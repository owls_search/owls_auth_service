# Owliphy php API service

## cron for sending mails
$ cd /app && php artisan saved-searches:send >> /dev/null 2>&1

## cron for creating feed
$ cd /app && php artisan google-feed:generate >> /dev/null 2>&1

## Artisan commands

`php artisan serve` - start server\
`php artisan google-feed:generate` - generate-feed\
`yarn send-saved-mails` - send saved mails right now\
`yarn generate-feed` - generate feed on prod\
