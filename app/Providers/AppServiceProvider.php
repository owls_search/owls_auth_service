<?php

namespace App\Providers;

use App\BusinessCase\Token;
use App\BusinessCase\TokenInterface;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(TokenInterface::class, function($app) {
           return new Token(env('TOKEN_KEY'));
        });
    }
}
