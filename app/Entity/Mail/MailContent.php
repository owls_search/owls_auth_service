<?php

namespace App\Entity\Mail;

class MailContent {

    /** @var string */
    private $type;

    /** @var string */
    private $content;

    public function __construct(string $type, string $content) {
        $this->type    = $type;
        $this->content = $content;
    }

    /**
     * @param array $substitutions
     * @return string
     */
    public function getContent(array $substitutions = []): string {
        return $this->replaceSubstitutions($this->content, $substitutions);
    }

    /**
     * @return string
     */
    public function getType(): string {
        return $this->type;
    }

    /**
     * @param string $text
     * @param array $substitutions
     * @return string
     * @example
     * $this->replaceSubstitutions(
     *      '<h1>#TITLE#</h1>',
     *      [ '#TITLE#' => "Test" ]
     * )
     *
     */
    private function replaceSubstitutions(string $text, array $substitutions): string {
        return str_replace(array_keys($substitutions), array_values($substitutions), $text);
    }

}