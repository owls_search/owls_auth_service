<?php

namespace App\Entity\Mail;

class MailAddress {

    /** @var string */
    private $email;

    /** @var string */
    private $name;

    /**
     * From constructor.
     *
     * @param $email
     * @param $name
     */
    public function __construct(string $email, string $name = '') {
        $this->email = $email;
        $this->name  = $name;
    }

    public function getAddress(): array {
        return $this->name !== ''
            ? [$this->email => $this->name]
            : [$this->email];
    }
}