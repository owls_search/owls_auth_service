<?php

namespace App\Console\Commands;

use App\Notifications\ScheduleMailsError;
use App\Notifications\ScheduleMailsSent;
use App\Service\JobSearchService;
use App\Service\Mail\SavedSearchMailingService;
use App\Service\SavedSearchesService;
use Exception;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Console\Command;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Notification;
use Throwable;

class SendSavedSearches extends Command
{
    use Notifiable;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'saved-searches:send';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sends saved searches notifications to users';

    /** @var SavedSearchesService */
    private $savedSearchesService;

    /** @var JobSearchService */
    private $jobSearchService;

    /** @var SavedSearchMailingService */
    private $savedSearchesMailingService;

    /**
     * Create a new command instance.
     *
     * @param SavedSearchesService $savedSearchesService
     * @param SavedSearchMailingService $savedSearchesMailingService
     * @param JobSearchService $jobSearchService
     */
    public function __construct(
        SavedSearchesService $savedSearchesService,
        SavedSearchMailingService $savedSearchesMailingService,
        JobSearchService $jobSearchService
    ) {
        $this->savedSearchesService        = $savedSearchesService;
        $this->savedSearchesMailingService = $savedSearchesMailingService;
        $this->jobSearchService            = $jobSearchService;
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     * @throws GuzzleException
     * @throws Throwable
     */
    public function handle(): void {
        $weeklyMails  = $this->savedSearchesService->getWeeklyScheduleMails();
        $monthlyMails = $this->savedSearchesService->getMonthlyScheduleMails();

        $this->sendJobsMails(array_merge($weeklyMails, $monthlyMails));

        $sentMails = count($weeklyMails) + count($monthlyMails);
        $this->notify(new ScheduleMailsSent($sentMails));
    }

    /**
     * @param $mailsToSend
     * @throws GuzzleException
     * @throws Throwable
     */
    private function sendJobsMails($mailsToSend): void {
        try {
            foreach ($mailsToSend as $mail) {
                $offers = $this->getOffers($mail['query']);
                $this->savedSearchesMailingService->mail(
                    $mail['email'],
                    sprintf(
                        'Hey %s %s! Here are some jobs for %s',
                        $mail['name'],
                        $mail['last_name'],
                        $mail['query']
                    ),
                    [
                        'name'     => $mail['name'],
                        'lastName' => $mail['last_name'],
                        'query'    => $mail['query'],
                        'offers'   => $offers,
                    ]
                );
            }
        }
        catch (Exception $e) {
            $this->notify(new ScheduleMailsError($e->getMessage()));
        }
    }

    /**
     * @param $query
     * @return array
     * @throws GuzzleException
     * @throws Throwable
     */
    private function getOffers($query): array {
        $offers = $this->jobSearchService->getJobsFromQuery($query, 3);

        foreach ($offers as $key => $offer) {
            $offers[$key]['programming'] = $this->stringifyJobsTags($offer['programming'], 'Tech.stack');
            $offers[$key]['benefits']    = $this->stringifyJobsTags($offer['benefits'], 'Benefits');
            $offers[$key]['skills']      = $this->stringifyJobsTags($offer['skills'], 'Skills');
        }

        return $offers;
    }

    /**
     * @param $tags
     * @param $strType
     * @return string
     */
    private function stringifyJobsTags($tags, $strType): string {
        $tags = array_map(
            function ($tag) {
                return $tag['text'];
            }, $tags
        );

        return count($tags)
            ? sprintf('%s: %s', $strType, implode(', ', $tags))
            : '';
    }

    /**
     * Route notifications for the Slack channel.
     *
     * @param Notification $notification
     * @return string
     */
    public function routeNotificationForSlack($notification) {
        return 'https://hooks.slack.com/services/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';
    }
}
