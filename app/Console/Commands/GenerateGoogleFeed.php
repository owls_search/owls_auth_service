<?php

namespace App\Console\Commands;

use App\Service\ExportCSVService;
use App\Service\FeedOffersService;
use Illuminate\Console\Command;
use RuntimeException;

class GenerateGoogleFeed extends Command {
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'google-feed:generate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generates google feed';

    /** @var FeedOffersService */
    private $feedOffersService;

    /** @var ExportCSVService */
    private $exportCSVService;

    /**
     * @param FeedOffersService $feedOffersService
     * @param ExportCSVService $exportCSVService
     */
    public function __construct(
        FeedOffersService $feedOffersService,
        ExportCSVService $exportCSVService
    ) {
        $this->feedOffersService = $feedOffersService;
        $this->exportCSVService  = $exportCSVService;
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     * @throws RuntimeException
     */
    public function handle(): void {
        $this->exportCSVService->createTemp();
        $this->exportCSVService->addHeaders(
            [
                'Job ID',
                'Title',
                'Final URL',
                'Image URL',
                'Description',
                'Category',
                'Address',
                'Contextual keywords',
                'Tracking template',
                'Final mobile URL',
            ]
        );

        self::printMem();

        foreach ($this->feedOffersService->pullOfferFeeds() as $offersChunk) {
            $mappedOffers = array_map('self::mapToGoogleFeed', $offersChunk);
            $this->exportCSVService->append($mappedOffers);
            self::printMem();
        }

        self::printMem();
        $this->exportCSVService->publishFeed();
    }

    private static function mapToGoogleFeed($offer): array {
        $offer              = (array) $offer;
        $contextualKeywords = $offer['programming'] !== '' ? str_replace(',', ';', $offer['programming']) : '';

        return [
            'id'                 => $offer['id'],
            'title'              => $offer['title'],
            'finalUrl'           => sprintf('https://clusterjobs.com/offer/%s', $offer['id']),
            'imageUrl'           => strpos($offer['logo'], 'http') === 0 ? $offer['logo'] : sprintf('https://storage.googleapis.com/owliphy-storage-bucket/%s', $offer['logo']),
            'description'        => strlen($offer['mini_description']) > 900 ? substr($offer['mini_description'], 0, 900) . '...' : $offer['mini_description'],
            'category'           => 'technical',
            'address'            => $offer['location'],
            'contextualKeywords' => sprintf('software engineering jobs; it jobs; %s', $contextualKeywords),
            'trackingTemplate'   => '{lpurl}?usertype={_user}&utm_source=offer-{_adformatid}&utm_medium=google&utm_campaign=remarketing',
            'finalMobileUrl'     => sprintf('https://clusterjobs.com/offer/%s', $offer['id']),
        ];
    }

    private static function printMem() {
        /* Currently used memory */
        $mem_usage = memory_get_usage();

        /* Peak memory usage */
        $mem_peak = memory_get_peak_usage();

        print_r("The script is now using: " . round($mem_usage / 1024) . "KB of memory.\r\n");
        print_r("Peak usage: " . round($mem_peak / 1024) . "KB of memory.\r\n\r\n");
    }
}
