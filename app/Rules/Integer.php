<?php

namespace App\Rules;


use Illuminate\Contracts\Validation\Rule;

class Integer implements Rule
{
    /**
     * @param string $attribute
     * @param mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        /** @noinspection TypeUnsafeComparisonInspection */
        return (int) $value == $value && (int) $value > 0;
    }

    public function message()
    {
        return 'Not integer or 0.';
    }
}