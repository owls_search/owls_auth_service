<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class UserType implements Rule
{
    public const APPLICANT = 'applicant';
    public const RECRUITER = 'recruiter';
    /**
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value): bool
    {
        return \in_array($value, [self::APPLICANT, self::RECRUITER], false);
    }

    /**
     * @return string
     */
    public function message()
    {
        return 'Unknown user type.';
    }
}
