<?php
/**
 * Created by PhpStorm.
 * User: coongeek
 * Date: 02/10/2018
 * Time: 21:35
 */

namespace App\Rules;


use Illuminate\Contracts\Validation\Rule;

class Strong implements Rule
{
    const TOO_SHORT = 1;
    const MUST_HAVE_DIGIT = 2;
    const MUST_HAVE_LETTER = 3;

    private $outcome;

    public function passes($attribute, $value)
    {
        if (strlen($value) < 6) {
            $this->outcome = self::TOO_SHORT;
            return false;
        }

        if (!preg_match("#[0-9]+#", $value)) {
            $this->outcome = self::MUST_HAVE_DIGIT;
            return false;
        }

        if (!preg_match("#[a-zA-Z]+#", $value)) {
            $this->outcome = self::MUST_HAVE_LETTER;
            return false;
        }

        return true;
    }

    public function message()
    {
        switch ($this->outcome) {
            case self::TOO_SHORT:
                $msg = "Password too short!";
                break;
            case self::MUST_HAVE_DIGIT:
                $msg = "Password must include at least one number!";
                break;
            case self::MUST_HAVE_LETTER:
                $msg = "Password must include at least one letter!";
                break;
        }

        return $msg;
    }
}