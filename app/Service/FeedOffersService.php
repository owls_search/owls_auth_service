<?php

namespace App\Service;

use Generator;
use Illuminate\Support\Facades\DB;

class FeedOffersService {

    private const OFFERS_AMOUNT = 40000;

    private const CHUNK_SIZE = 1000;

    /**
     * @return Generator
     */
    public function pullOfferFeeds(): Generator {
        for ($i = 0; $i * self::CHUNK_SIZE < self::OFFERS_AMOUNT; $i++) {
            $sendQueries = DB::table('jobs')
                ->where('dynamic_bid', '>', 0.19)
                ->orderBy('dynamic_bid', 'DESC')
                ->offset($i * self::CHUNK_SIZE)
                ->limit(self::CHUNK_SIZE)
                ->get()
            ;

            yield $sendQueries->toArray();
        }
    }
}
