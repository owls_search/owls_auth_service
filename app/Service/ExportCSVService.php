<?php

namespace App\Service;

use Exception;
use RuntimeException;

class ExportCSVService {

    /** @var FileStorageService */
    private $fileStorage;

    private $writeBuffer = [];

    private $writeBufferCounter = 0;

    private const WRITE_LIMIT = 50;

    private const FILE_PATH = __DIR__ . '/../../export/feed/temp.csv';

    /**
     * @param FileStorageService $fileStorage
     */
    public function __construct(FileStorageService $fileStorage) {
        $this->fileStorage = $fileStorage;
    }

    public function createTemp(): void {
        if (!file_exists(dirname(self::FILE_PATH))) {
            if (!mkdir($concurrentDirectory = dirname(self::FILE_PATH), 0777, true) && !is_dir($concurrentDirectory)) {
                throw new RuntimeException(sprintf('Directory "%s" was not created', $concurrentDirectory));
            }
        }
    }

    public function addHeaders($headers): void {
        $resource = fopen(self::FILE_PATH, 'wb');
        fputcsv($resource, (array) $headers);
        fclose($resource);
    }

    public function append($offersChunk): void {
        foreach ($offersChunk as $offer) {
            $this->writeLine($offer);
        }
    }

    public function publishFeed(): void {
        try {
            $this->fileStorage->delete('feed.csv', FileStorageService::COMPANY_BUCKET_NAME);
        }
        catch (Exception $e) {
        };
        $this->fileStorage->upload(file_get_contents(self::FILE_PATH), 'feed.csv', FileStorageService::COMPANY_BUCKET_NAME);
    }

    private function writeLine($data): void {
        $this->writeBuffer[] = $data;
        $this->writeBufferCounter++;

        if ($this->writeBufferCounter >= self::WRITE_LIMIT) {
            $resource = fopen(self::FILE_PATH, 'ab+');
            fwrite($resource, self::getCSVLine($this->writeBuffer));
            fclose($resource);
            $this->writeBuffer        = [];
            $this->writeBufferCounter = 0;
        }
    }

    /**
     * Convert a multi-dimensional, associative array to CSV data
     *
     * @param array $data the array of data
     * @return string       CSV text
     */
    private static function getCSVLine($data): string {
        $fh = fopen('php://temp', 'rwb');

        foreach ($data as $row) {
            fputcsv($fh, $row);
        }
        rewind($fh);
        $csv = stream_get_contents($fh);
        fclose($fh);

        return $csv;
    }
}
