<?php

namespace App\Service;

use App\Exceptions\SearchResponseException;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Psr7\MultipartStream;
use Throwable;

class JobSearchService {

    private const API_KEY = 'a1d07162dad914f78c972b6ece1549d9';

    private const API_ENDPOINT = 'https://search.clusterjobs.de/v1/listjobs/';

    /** @var Client */
    private $client;

    public function __construct() {
        $this->client = new Client();
    }

    /**
     * @param string $query
     * @param int $size
     * @return array
     * @throws Throwable
     * @throws GuzzleException
     */
    public function getJobsFromQuery(
        string $query,
        int $size
    ): array {
        return $this->getJobs($query, $size);
    }

    /**
     * @param string $query
     * @param int $size
     * @param array $additionalParams
     * @return array
     * @throws Throwable
     * @throws GuzzleException
     */
    private function getJobs(
        string $query,
        int $size,
        array $additionalParams = []
    ): array {
        $requestParams = [
            'page_size'  => $size,
            'page_index' => 0,
            'language'   => 'all',
            'value'      => $query,
            'API-KEY'    => self::API_KEY,
        ];

        $requestParams = array_merge($requestParams, $additionalParams);

        $boundary  = 'multipart_boundary';
        $multipart = [
//            [
//                'name'     => 'file',
//                'contents' => fopen('test.pdf', 'r'),
//                'headers'  => [ 'Content-Type' => 'application/octet-stream']
//            ],
        ];
        $multipart = array_merge($multipart, $this->toFormDataFields($requestParams));

        $params = [
            'headers' => [
                'Content-Type' => 'multipart/form-data; boundary=' . $boundary,
            ],
            'body'    => new MultipartStream($multipart, $boundary),
        ];

        $response = $this->client->request(
            'POST',
            self::API_ENDPOINT,
            $params
        );
        throw_if($response->getStatusCode() !== 200, new SearchResponseException('Search api unexpected response'));

        $response = json_decode($response->getBody(), true);
        throw_if(!array_key_exists('jobs', $response), new SearchResponseException('Search api returned empty response'));

        return $response['jobs'];
    }

    /**
     * @param $params
     * @return array
     */
    private function toFormDataFields($params): array {
        $formDataParams = [];

        foreach ($params as $param => $value) {
            $formDataParams[] = [
                'name'     => $param,
                'contents' => $value,
            ];
        }

        return $formDataParams;
    }
}
