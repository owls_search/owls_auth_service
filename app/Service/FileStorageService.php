<?php

namespace App\Service;

use App\Exceptions\FileStorageException;
use Exception;
use Google\Cloud\Storage\StorageClient;
use Illuminate\Http\UploadedFile;
use InvalidArgumentException;
use function is_object;
use function is_string;

class FileStorageService {
    private const PROJECT_NAME = 'reliable-proton-251017';

    public const BUCKET_NAME = 'clujsterjobs-website-storage';
    public const COMPANY_BUCKET_NAME = 'company-public';
    private const GS_URL = 'gs://clujsterjobs-website-storage';
    private const GS_SITEMAP_URL = 'gs://clusterjobs-sitemap-bucket';

    /** @var StorageClient|null */
    private $storage = null;

    private function getStorage(): StorageClient {
        if (!is_object($this->storage)) {
            $this->storage = new StorageClient(
                [
                    'projectId' => self::PROJECT_NAME,
                ]
            );
        }

        return $this->storage;
    }

    /**
     * @param UploadedFile $file
     * @return string
     * @throws FileStorageException
     */
    public function uploadFromRequest(UploadedFile $file = null): string {
        if (!is_object($file) || $file === null) {
            return '';
        }

        try {
            $name = md5(uniqid(mt_rand(), true));
            return $this->upload($file->get(), $name);
        }
        catch (Exception $e) {
            throw new FileStorageException($e->getMessage());
        }
    }

    /**
     * @param string $fileContents
     * @param string $name
     * @param string $bucketName
     * @return string
     * @throws InvalidArgumentException
     */
    public function upload(string $fileContents, string $name, string $bucketName = self::BUCKET_NAME): string {
        $bucket = $this->getStorage()->bucket($bucketName);
        $object = $bucket->upload(
            $fileContents, [
            'name' => $name,
        ]
        );

        return $object->name();
    }

    /**
     * @param string $fileName
     * @param string $bucketName
     * @throws FileStorageException
     */
    public function delete(string $fileName, string $bucketName = self::BUCKET_NAME): void {
        $bucket = $this->getStorage()->bucket($bucketName);
        if ('' === $fileName || $fileName === null) {
            return;
        }
        try {
            $object = $bucket->object($fileName);
            $object->delete();
        }
        catch (Exception $e) {
            throw new FileStorageException($e->getMessage());
        }
    }

    /**
     * @param string $fileName
     * @return string
     * @throws FileStorageException
     */
    public function getFilePath(string $fileName): string {
        try {
            if ('' === $fileName || $fileName === null) {
                throw new FileStorageException('File name is empty!');
            }

            $this->getStorage()->registerStreamWrapper();
            return sprintf('%s/%s', self::GS_URL, $fileName);
        }
        catch (Exception $e) {
            throw new FileStorageException($e->getMessage());
        }
    }

    /**
     * @param string $fileName
     * @return string
     * @throws FileStorageException
     */
    public function getSitemapPath(string $fileName): string {
        try {
            if ('' === $fileName || $fileName === null) {
                throw new FileStorageException('File name is empty!');
            }

            $this->getStorage()->registerStreamWrapper();
            return sprintf('%s/%s', self::GS_SITEMAP_URL, $fileName);
        }
        catch (Exception $e) {
            throw new FileStorageException($e->getMessage());
        }
    }

    public function needsUpdate($fileParameter): bool {
        return !is_string($fileParameter);
    }

    /**
     * @param $fileName
     * @param UploadedFile $file
     * @return bool
     * @throws FileStorageException
     */
    public function deleteFileAndIsUpdateNeeded($fileName, UploadedFile $file = null): bool {
        if ('' === (string) $fileName || $fileName === null) {
            // Return true if current is non but there is something to upload
            return $file !== null;
        }

        // Delete old file anyway, but return true only if need to upload
        $this->delete($fileName);
        return $file !== null;
    }
}
