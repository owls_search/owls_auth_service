<?php

namespace App\Service;

use Illuminate\Support\Facades\DB;

class SavedSearchesService {

    private const WEEKLY_FIELDS = [
        'send_weekly_monday',
        'send_weekly_tuesday',
        'send_weekly_wednesday',
        'send_weekly_thursday',
        'send_weekly_friday',
        'send_weekly_saturday',
        'send_weekly_sunday',
    ];

    public function addSavedSearch(
        int $userId,
        string $query,
        bool $isWeekly,
        array $dayOfMonth,
        array $dayOfWeek
    ): int {
        $insertData = $this->getSendPeriodData($isWeekly, $dayOfMonth, $dayOfWeek);
        $insertData['user_id'] = $userId;
        $insertData['query'] = $query;

        return DB::table('saved_searches')
            ->insertGetId($insertData);
    }

    public function getWeeklyScheduleMails(): array {
        $curDayField = self::WEEKLY_FIELDS[date('N') - 1];

        $sendQueries = DB::table('saved_searches')
            ->select(
                [
                    'users.name',
                    'users.last_name',
                    'users.email',
                    'saved_searches.query',
                ]
            )
            ->join('users', 'saved_searches.user_id', '=', 'users.id')
            ->where(sprintf('saved_searches.%s', $curDayField), '=', 1)
            ->get()
        ;

        return array_map(
            function ($savedSearch) {
                return (array) $savedSearch;
            }, $sendQueries->toArray()
        );
    }

    public function getMonthlyScheduleMails(): array {
        $curDateField = date('d');
        $sendQueries  = DB::table('saved_searches')
            ->select(
                [
                    'users.name',
                    'users.last_name',
                    'users.email',
                    'saved_searches.query',
                ]
            )
            ->join('users', 'saved_searches.user_id', '=', 'users.id')
            ->where('saved_searches.send_date_of_month', 'LIKE', '%' . $curDateField . '%')
            ->get()
        ;

        return array_map(
            function ($savedSearch) {
                return (array) $savedSearch;
            }, $sendQueries->toArray()
        );
    }

    private function getSendPeriodData(
        bool $isWeekly,
        array $dayOfMonth,
        array $dayOfWeek
    ) : array {
        if (!$isWeekly) {
            $leadingZeroDates = array_map(
                static function ($date) {
                    return sprintf('%02d', $date);
                }, $dayOfMonth
            );

            return [
                'send_date_of_month' => implode(',', $leadingZeroDates),
            ];
        }

        $insertRow = [];

        foreach ($dayOfWeek as $selectedDay) {
            $selectedDayField = sprintf('send_weekly_%s', strtolower($selectedDay));

            if (in_array($selectedDayField, self::WEEKLY_FIELDS, true)) {
                $insertRow[$selectedDayField] = true;
            }
        }

        return $insertRow;
    }
}