<?php

namespace App\Service\Mail;

use App\Entity\Mail\MailAddress;
use App\Entity\Mail\MailContent;
use App\Service\MailService;

class RegisterUserFromNewcomerMailingService implements MailingServiceInterface {

    /** @var MailService */
    private $mailService;

    /** @var MailAddress */
    private $from;

    /** @var MailContent */
    private $plainContent;

    public function __construct(MailService $mailService) {
        $this->mailService  = $mailService;
        $this->from         = new MailAddress('no-reply@clusterjobs.com', 'Owliphy Team');
        $this->plainContent = new MailContent('text/plain', 'Your search have been saved');
    }

    /**
     * @param string $toMail
     * @param string $subject
     * @param array $mailVariables
     * @return void
     */
    public function mail(
        string $toMail,
        string $subject,
        array $mailVariables
    ): void {
        $to = new MailAddress($toMail);
        $this->mailService->send(
            $this->from,
            $to,
            $subject,
            $this->plainContent,
            $this->getMail(),
            [
                '#SEARCH_QUERY#' => $mailVariables['query'],
                '#LOGIN#'        => $mailVariables['email'],
                '#PASSWORD#'     => $mailVariables['password'],
            ]
        );
    }

    public function getMail(): MailContent {
        return new MailContent('text/html', file_get_contents(__DIR__ . '/templates/html/newUserFromEmail.html'));
    }
}
