<?php

namespace App\Service\Mail;

use App\Entity\Mail\MailAddress;
use App\Entity\Mail\MailContent;
use App\Service\MailService;

class FeedbackMailingService implements MailingServiceInterface {

    /** @var MailService */
    private $mailService;

    /** @var MailAddress */
    private $from;

    /** @var MailAddress */
    private $toOwliphy;

    /** @var MailContent */
    private $plainContent;

    public function __construct(MailService $mailService) {
        $this->mailService  = $mailService;
        $this->from         = new MailAddress('feedback@clusterjobs.com', 'Owliphy Team');
        $this->toOwliphy    = new MailAddress('feedback@clusterjobs.com', 'Contact Owliphy');
        $this->plainContent = new MailContent('text/plain', 'Feedback message has been sent!');
    }

    /**
     * @param string $toMail
     * @param string $subject
     * @param array $mailVariables
     * @return void
     */
    public function mail(
        string $toMail,
        string $subject,
        array $mailVariables
    ): void {
        $to = new MailAddress($toMail);

        $this->mailService->send(
            $this->from,
            $this->toOwliphy,
            $subject,
            $this->plainContent,
            $this->getOwliphyMail(),
            [
                '#USER_MAIL#' => $mailVariables['email'],
                '#MESSAGE#'   => $mailVariables['message'],
            ]
        );
        $this->mailService->send($this->from, $to, $subject, $this->plainContent, $this->getMail());
    }

    public function getMail(): MailContent {
        return new MailContent('text/html', file_get_contents(__DIR__ . '/templates/html/feedback.html'));
    }

    public function getOwliphyMail(): MailContent {
        return new MailContent('text/html', file_get_contents(__DIR__ . '/templates/html/feedbackToOwliphy.html'));
    }
}
