<?php

namespace App\Service\Mail;

use App\Entity\Mail\MailAddress;
use App\Entity\Mail\MailContent;
use App\Service\MailService;

class SavedSearchMailingService implements MailingServiceInterface {

    /** @var MailService */
    private $mailService;

    /** @var MailAddress */
    private $from;

    /** @var string */
    private $utmPixel;

    public function __construct(MailService $mailService) {
        $this->mailService = $mailService;
        $this->from        = new MailAddress('no-reply@clusterjobs.com', 'Owliphy Team');
        $this->utmPixel    = '?utm_campaign=saved_search&utm_medium=mail&utm_source=mail_jobs_details_' . date('dmY');
    }

    /**
     * @param string $toMail
     * @param string $subject
     * @param array $mailVariables
     * @return void
     */
    public function mail(
        string $toMail,
        string $subject,
        array $mailVariables
    ): void {
        $to           = new MailAddress($toMail);
        $plainContent = new MailContent(
            'text/plain',
            str_replace('#QUERY#', $mailVariables['query'], 'New jobs in Germany for "#QUERY#"')
        );

        $offersListHtml = $this->generateOffersHtml($mailVariables['offers']);

        $this->mailService->send(
            $this->from,
            $to,
            $subject,
            $plainContent,
            $this->getMail(),
            [
                '#NAME#'        => sprintf('%s %s', $mailVariables['name'], $mailVariables['lastName']),
                '#QUERY#'       => $mailVariables['query'],
                '#QUERY_LINK#'  => sprintf('https://clusterjobs.com/search/%s%s', $mailVariables['query'], $this->utmPixel),
                '#OFFERS_LIST#' => $offersListHtml,
            ]
        );
    }

    public function getMail(): MailContent {
        return new MailContent('text/html', file_get_contents(__DIR__ . '/templates/static_html/savedSearch.html'));
    }

    private function generateOffersHtml(array $offers): string {
        $html = '';
        foreach ($offers as $offer) {
            $offerHtml = file_get_contents(__DIR__ . '/templates/static_html/offer.html');
            $offerHtml = str_replace(
                [
                    '#TITLE#',
                    '#COMPANY#',
                    '#LOCATION#',
                    '#BENEFITS#',
                    '#TECH_STACK#',
                    '#SKILLS#',
                    '#DETAILS_LINK#',
                    '#LINK#',
                ],
                [
                    $offer['title'],
                    $offer['name'] !== '' ? sprintf('for %s', $offer['name']) : '',
                    $offer['location'] !== '' ? sprintf('in %s', $offer['location']) : '',
                    $offer['benefits'],
                    $offer['programming'],
                    $offer['skills'],
                    sprintf('https://clusterjobs.com/offer/%s%s', $offer['id'], $this->utmPixel),
                    $offer['url'],
                ],
                $offerHtml
            );
            $html      .= $offerHtml;
        }

        return $html;
    }
}
