<?php

namespace App\Service;

use App\Entity\Mail\MailAddress;
use App\Entity\Mail\MailContent;
use Swift_Mailer;
use Swift_Message;
use Swift_SmtpTransport;

class MailService {

    /** @var Swift_Mailer */
    private $mailer;

    public function __construct() {
        $transport = new Swift_SmtpTransport('smtp.mailgun.org', 25);
        $transport->setUsername(getenv('MAILGUN_SMTP_USERNAME'));
        $transport->setPassword(getenv('MAILGUN_SMTP_PASSWORD'));

        $this->mailer = new Swift_Mailer($transport);
    }

    public function send(
        MailAddress $fromMail,
        MailAddress $toMail,
        string $subject,
        MailContent $text,
        MailContent $content,
        array $substitutions = []
    ): int {
        $message = new Swift_Message($subject);

        $message->setFrom($fromMail->getAddress());
        $message->setTo($toMail->getAddress());
        $message->setBody($content->getContent($substitutions), $content->getType());
        $message->addPart($text->getContent($substitutions), $text->getType());

        return $this->mailer->send($message);
    }
}