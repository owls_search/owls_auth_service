<?php
namespace App\Service\Mail;

use App\Entity\Mail\MailContent;

interface MailingServiceInterface {

    public function mail(
        string $toMail,
        string $subject,
        array $mailVariables
    );

    public function getMail(): MailContent;
}