<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;

class ExportController {

    private const FILE_PATH = __DIR__ . '/../../../../export/feed/feed.csv';

    public function getGoogleFeed(Request $request) {
        $validator = Validator::make(
            $request->all(), [
                'key' => ['bail'],
            ]
        );

        if (
            strpos($request->server('HTTP_REFERRER'), 'clusterjobs.com/') === false
            && $request->get('key') !== getenv('GOOGLE_FEED_KEY')
        ) {
            return json_encode(['message' => 'Wrong API key']);
        }

        if ($validator->fails()) {
            return json_encode(['errors' => json_encode($validator->errors())]);
        }

        if (!file_exists(self::FILE_PATH)) {
            return json_encode(['message' => 'File does not exists']);
        }

        $headers = [
            'Content-Type' => 'text/csv',
        ];

        return Response::download(self::FILE_PATH, 'feed.csv', $headers);
    }
}
