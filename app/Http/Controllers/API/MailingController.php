<?php

namespace App\Http\Controllers\API;

use App\Service\Mail\FeedbackMailingService;
use App\Service\Mail\MailingServiceInterface;
use App\Service\Mail\NewSavedSearchMailingService;
use App\Service\Mail\RegisterUserFromNewcomerMailingService;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use InvalidArgumentException;

class MailingController {

    /** @var FeedbackMailingService */
    private $feedbackMailingService;

    /** @var NewSavedSearchMailingService */
    private $newSavedSearchMailingService;

    /** @var RegisterUserFromNewcomerMailingService */
    private $registerUserFromNewcomerMailingService;

    public function __construct(
        FeedbackMailingService $feedbackMailingService,
        NewSavedSearchMailingService $newSavedSearchMailingService,
        RegisterUserFromNewcomerMailingService $registerUserFromNewcomerMailingService
    ) {
        $this->feedbackMailingService                 = $feedbackMailingService;
        $this->newSavedSearchMailingService           = $newSavedSearchMailingService;
        $this->registerUserFromNewcomerMailingService = $registerUserFromNewcomerMailingService;
    }

    /**
     * @param string $type
     * @return MailingServiceInterface|null
     */
    private function getServiceByType(string $type): ?MailingServiceInterface {
        $serviceByType = [
            'feedback'            => $this->feedbackMailingService,
            'newSavedSearch'      => $this->newSavedSearchMailingService,
            'newcomerSavedSearch' => $this->registerUserFromNewcomerMailingService,
        ];

        if (!array_key_exists($type, $serviceByType)) {
            return null;
        }

        return $serviceByType[$type];
    }

    public function internalSendMail(Request $request) {
        try {
            $validator = Validator::make(
                $request->all(), [
                    'key'       => ['bail', 'required'],
                    'to'        => ['bail', 'required'],
                    'subject'   => ['bail', 'required'],
                    'variables' => ['bail', 'required'],
                    'type'      => ['bail', 'required'],
                ]
            );

            if ($validator->fails()) {
                throw new InvalidArgumentException(json_encode($validator->errors()));
            }

            if ($request->get('key') !== getenv('API_KEY')) {
                return json_encode(['message' => 'Wrong API key']);
            }

            if ($mailingService = $this->getServiceByType($request->get('type'))) {
                $mailingService->mail(
                    $request->get('to'),
                    $request->get('subject'),
                    $request->get('variables')
                );

                return response()->json(true);
            }

            throw new InvalidArgumentException('message type not found');
        }
        catch (Exception $e) {
            return response()->json(
                [
                    'message' => 'Mailing failed',
                    'errors'  => $e->getMessage(),
                ], 500
            );
        }
    }
}
