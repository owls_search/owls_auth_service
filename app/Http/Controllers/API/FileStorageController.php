<?php

namespace App\Http\Controllers\API;

use App\Service\FileStorageService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class FileStorageController {

    /** @var FileStorageService */
    private $fileStorage;

    public function __construct(
        FileStorageService $fileStorageService
    ) {
        $this->fileStorage = $fileStorageService;
    }

    public function storeFile(Request $request) {
        $validator = Validator::make(
            $request->all(), [
                'key'  => ['bail'],
                'file' => ['bail', 'required'],
        ]
        );

        if ($validator->fails()) {
            return ['errors' => json_encode($validator->errors())];
        }

        $fileName = null;
        $file     = $request->file('file');
        $fileName = $this->fileStorage->uploadFromRequest($file);

        return response()->json(['name' => $fileName]);
    }

    public function deleteFile(Request $request) {
        $validator = Validator::make(
            $request->all(), [
                'key'      => ['bail', 'required'],
                'fileName' => ['bail', 'required'],
        ]
        );

        if (
            strpos($request->server('HTTP_REFERRER'), 'clusterjobs.com/') === false
            && $request->get('key') !== getenv('API_KEY')
        ) {
            return json_encode(['message' => 'Wrong API key']);
        }

        if ($validator->fails()) {
            return ['errors' => json_encode($validator->errors())];
        }

        $fileName = null;
        $file     = $request->get('fileName');
        $this->fileStorage->delete($file);

        return response()->json(true);
    }

    public function getFile(Request $request) {
        $validator = Validator::make(
            $request->all(), [
                'key'      => ['bail'],
                'fileName' => ['bail', 'required'],
        ]
        );

        if (
            strpos($request->server('HTTP_REFERRER'), 'clusterjobs.com/') === false
            && $request->get('key') !== getenv('API_KEY')
        ) {
            return json_encode(['message' => 'Wrong API key']);
        }

        if ($validator->fails()) {
            return ['errors' => json_encode($validator->errors())];
        }

        $fileName = $request->get('fileName');
        return response()->file($this->fileStorage->getFilePath($fileName));
    }

    public function getSitemap($fileName) {
        return response()->file($this->fileStorage->getSitemapPath($fileName), [
            'Content-Type' => 'application/xml'
        ]);
    }
}
