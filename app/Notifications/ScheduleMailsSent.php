<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Channels\SlackWebhookChannel;
use Illuminate\Notifications\Messages\SlackMessage;
use Illuminate\Notifications\Notification;

class ScheduleMailsSent extends Notification {
    use Queueable;

    /** @var int */
    private $mailsSent;

    /**
     * Create a new notification instance.
     *
     * @param int $mailsSent
     */
    public function __construct(int $mailsSent) {
        $this->mailsSent = $mailsSent;
    }

    /**
     * Get the Slack representation of the notification.
     *
     * @param mixed $notifiable
     * @return SlackMessage
     */
    public function toSlack($notifiable) {
        return (new SlackMessage)
            ->content(sprintf('%s schedule mail has been sent', $this->mailsSent));
    }

    /**
     * Get the notification channels.
     *
     * @param mixed $notifiable
     * @return array|string
     */
    public function via($notifiable) {
        return [SlackWebhookChannel::class];
    }
}
