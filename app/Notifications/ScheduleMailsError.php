<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Channels\SlackWebhookChannel;
use Illuminate\Notifications\Messages\SlackMessage;
use Illuminate\Notifications\Notification;

class ScheduleMailsError extends Notification {
    use Queueable;

    /** @var string */
    private $message;

    /**
     * Create a new notification instance.
     *
     * @param string $message
     */
    public function __construct(string $message) {
        $this->message = $message;
    }

    /**
     * Get the Slack representation of the notification.
     *
     * @param mixed $notifiable
     * @return SlackMessage
     */
    public function toSlack($notifiable) {
        return (new SlackMessage)
            ->content(sprintf('Couldn`t sent schedule mails: %s', $this->message));
    }

    /**
     * Get the notification channels.
     *
     * @param mixed $notifiable
     * @return array|string
     */
    public function via($notifiable) {
        return [SlackWebhookChannel::class];
    }
}
