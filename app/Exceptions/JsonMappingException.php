<?php

namespace App\Exceptions;

use RuntimeException;
use Throwable;

class JsonMappingException extends RuntimeException {
    public $responseStatus;

    public function __construct(string $message = "", int $responseStatus = 400, int $code = 0, Throwable $previous = null) {
        parent::__construct($message, $code, $previous);
        $this->responseStatus = $responseStatus;
    }

    public function toJsonResponse(): array {
        return [
            [
                'message' => $this->message,
            ],
            $this->responseStatus,
        ];
    }
}