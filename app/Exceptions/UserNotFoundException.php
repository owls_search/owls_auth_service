<?php

namespace App\Exceptions;

class UserNotFoundException extends JsonMappingException {
}