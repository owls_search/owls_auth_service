<?php

namespace App\Entity\Mail;

use PHPUnit\Framework\TestCase;
use Throwable;

class MailContentTest extends TestCase {

    /**
     * @test
     * Get type test
     *
     * @throws Throwable
     */
    public function getTypeTest() {
        $type    = str_random();
        $content = new MailContent($type, 'random content');
        $this->assertSame($content->getType(), $type);
    }

    /**
     * @test
     * Replacing substitutions test
     *
     * @throws Throwable
     */
    public function getContentTest() {
        $content  = 'BLAHALBA#TEST#TEST#';
        $expected = 'BLAHALBAwoofTEST#';
        $content  = new MailContent(str_random(), $content);
        $this->assertSame($content->getContent(['#TEST#' => 'woof']), $expected);
    }
}
