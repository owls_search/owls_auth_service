<?php

namespace App\Service;

use GuzzleHttp\Exception\GuzzleException;
use PHPUnit\Framework\ExpectationFailedException;
use PHPUnit\Framework\TestCase;
use SebastianBergmann\RecursionContext\InvalidArgumentException;
use Throwable;

class JobSearchServiceTest extends TestCase {

    /**
     * @test
     * Check api response
     *
     * @throws ExpectationFailedException
     * @throws InvalidArgumentException
     * @throws Throwable
     * @throws GuzzleException
     */
    public function getJobsFromQueryTest(): void {
        $query   = 'php developer';
        $service = new JobSearchService();
        $jobs    = $service->getJobsFromQuery($query, 5);
        $this->assertTrue(count($jobs) > 0);
    }
}
